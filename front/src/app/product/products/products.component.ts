import { Component, OnInit } from '@angular/core';
import {Product} from '../models/product.model';
import {Observable} from 'rxjs';
import {ProductService} from '../services/product.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent {

  products$!: Observable<Product[]>;
  constructor(private productService: ProductService, private router: Router) { }

  ngOnInit() {
    this.products$ = this.productService.getAllProducts();
  }


  /*onClick() {
    this.router.navigateByUrl('/buyProduct');
  }*/

}
