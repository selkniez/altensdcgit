import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import { Product } from '../models/product.model';
import {environnement} from "../../environnement/environnement";


@Injectable({
  providedIn: 'root'
})

export class ProductService{

  constructor(private http: HttpClient) {
  }

  getAllProducts(): Observable<Product[]>{
    return this.http.get<Product[]>(`${environnement.apiUrl}/get`);
  }
}
